﻿ /*
 * File      : iap.c
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * zengxiaohui43@163.com
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 */
 #include "iap.h" 
 #include "stdio.h" 
#include <string.h>
 
APP_PARA_TypeDef ota_app_para;

 #define TRACE_ENABLE

#ifdef TRACE_ENABLE

#define LOG 		printf  
#else	
#define LOG(x)
#endif
 
 /*
*********************************************************************************************************
*	函 数 名: JumpToAPP
*	功能说明: 跳转到系统APP
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void JumpToAPP(void)
{
	
	void (*jump2app)(void);        /* 声明一个函数指针 */
	__IO uint32_t app_addr = 0; /* STM32F4的系统APP地址 */

//	/* 关闭全局中断 */
//	DISABLE_INT(); 

//	/* 关闭滴答定时器，复位到默认值 */
//	SysTick->CTRL = 0;
//	SysTick->LOAD = 0;
//	SysTick->VAL = 0;

//	/* 设置所有时钟到默认状态，使用HSI时钟 */
//	HAL_RCC_DeInit();

//	/* 关闭所有中断，清除所有中断挂起标志 */
//	for (i = 0; i < 8; i++)
//	{
//		NVIC->ICER[i]=0xFFFFFFFF;
//		NVIC->ICPR[i]=0xFFFFFFFF;
//	}	

//	/* 使能全局中断 */
//	ENABLE_INT();
	
	app_addr=FLASH_APP_ADDR;
	
	/* 判断复位程序地址是否为0X08XXXXXX */
	if(((*(volatile unsigned int *)(app_addr + 4)) & 0xFF000000) == 0x08000000)	
	{
		/* 检查栈顶地址是否合法 */
		if(((*(volatile unsigned int *)app_addr) & 0x2FFE0000) == 0x20000000)	
		{
			/* 跳转到APP，首地址是MSP，地址+4是复位中断服务程序地址 */
			jump2app = (void (*)(void)) (*((uint32_t *) (app_addr + 4)));

			/* 设置主堆栈指针 */
			__set_MSP(*(uint32_t *)app_addr);
			
//			/* 在RTOS工程，这条语句很重要，设置为特权级模式，使用MSP指针 */
//			__set_CONTROL(0);

			/* 跳转到系统APP */
			jump2app(); 
		}
	}
}
 
 
/*************************************************************
*	函数名称：	copy_firmware_data
*
*	函数功能：	将下载好的固件数据拷贝到程序区进行升级
*
*	入口参数：	file_len：文件长度
*
*	返回参数：	1-OK    0-FAIL
*
*	说明：		
*************************************************************/
void copy_firmware_data(uint32_t file_len)
{
	uint32_t app_addr,frimware_addr;
	uint32_t len = 0;
	uint16_t pack_size=W25Q128FV_SUBSECTOR_SIZE;//单次读写的数据长度
	uint8_t read_data_buff[W25Q128FV_SUBSECTOR_SIZE];
	app_addr = FLASH_APP_ADDR;			//写入实际地址
	frimware_addr =FIRMWARE_SAVE_ADDR;	//fw存放地址

	while(len<file_len)
	{
		if(file_len-len<W25Q128FV_SUBSECTOR_SIZE)//最后一包数据且小于W25Q128_SECTOR_SIZE
		{
			pack_size=file_len-len;
		}
		
		bsp_w25qx_read(read_data_buff,frimware_addr,pack_size);//读取flash内数据

		STMFLASH_Write(app_addr,(uint32_t*)read_data_buff,pack_size);//写入APP区
		
		app_addr+=pack_size;
		len+=pack_size;
		frimware_addr+=pack_size;
	}

}


/*************************************************************
*	函数名称：	get_pack_sumer
*
*	函数功能：	将数据包累加
*
*	入口参数： *data：累加数据，len-数据长度
*
*	返回参数：	累加和
*
*	说明：		
*************************************************************/
uint16_t get_pack_sumer(uint8_t* data,uint16_t len)
{
	uint16_t sumer=0;
	int i;
	for(i=0;i<len;i++)
	{
		sumer += data[i];
	}
	return sumer;
}

/*************************************************************
*	函数名称：	check_date_sumer_mcu
*
*	函数功能：	将数据包累加校验
*
*	入口参数： data_addr，MCU数据开始地址,data_len-数据包长度，check_sumer-正确的校验和
*
*	返回参数：	1-校验成功  ，0-校验失败
*
*	说明：		
*************************************************************/
uint8_t check_date_sumer_mcu(uint32_t data_addr,uint32_t data_len,uint16_t check_sumer)
{
	uint32_t  Flash_read_buf[MCU_PAGE_SIZE/4];
	uint32_t len = 0;
	uint16_t read_len =MCU_PAGE_SIZE;
	uint16_t sumer = 0;
	while(len<data_len)
	{
		if(data_len-len<MCU_PAGE_SIZE)
		{
			read_len = data_len-len ;
		}
		
		STMFLASH_Read(data_addr,Flash_read_buf, read_len);//读取flash内数据
		sumer += get_pack_sumer((uint8_t*)Flash_read_buf,read_len);
		data_addr+=read_len;
		len += read_len;
	}
	LOG("sumer:0x%x\r\n",sumer);
	if(sumer == check_sumer)
	{
		ota_app_para.check_sumer[0]=sumer>>8;
        ota_app_para.check_sumer[1]=sumer;
		return 1;
		
	}
	else
	{
		return 0;
	}
}


/*************************************************************
*	函数名称：	check_date_sumer_flash
*
*	函数功能：	将数据包累加校验
*
*	入口参数： data_addr，外部flash数据开始地址,data_len-数据包长度，check_sumer-正确的校验和
*
*	返回参数：	1-校验成功  ，0-校验失败
*
*	说明：		
*************************************************************/
#if 1
uint8_t check_date_sumer_flash(uint32_t data_addr,uint32_t data_len,uint16_t check_sumer)
{
	uint32_t len = 0;
	uint16_t read_len =W25Q128FV_SUBSECTOR_SIZE;
	uint16_t sumer = 0;
	uint8_t  read_data_buff[W25Q128FV_SUBSECTOR_SIZE];
	while(len<data_len)
	{
		if(data_len-len<W25Q128FV_SUBSECTOR_SIZE)
		{
			read_len = data_len-len ;
		}
		
		bsp_w25qx_read(read_data_buff,data_addr, read_len);//读取flash内数据
		sumer += get_pack_sumer(read_data_buff,read_len);
		data_addr+=read_len;
		len += read_len;
	}
	LOG("sumer:0x%x",sumer);
	if(sumer == check_sumer)
	{
		ota_app_para.check_sumer[0]=sumer>>8;
        ota_app_para.check_sumer[1]=sumer;
		return 1;
		
	}
	else
	{
		return 0;
	}
}
#endif
/*************************************************************
*	函数名称：	firmware_upgrade
*
*	函数功能：	写入缓存的程序，并且检验
*
*	入口参数： 无
*
*	返回参数：	1-校验成功  ，0-校验失败
*
*	说明：		
*************************************************************/
uint8_t firmware_upgrade(void)
{
	uint8_t i=0;
	uint32_t update_len;
	uint16_t update_sumer;
	update_len =ota_app_para.file_len;
	update_sumer = ((ota_app_para.check_sumer[0]<<8)+ota_app_para.check_sumer[1]);
	LOG("file size=%d\r\nupdate sumer=0x%x\r\n",update_len,update_sumer);

	for(i=0;i<10;i++)		//程序写入错误有10次重新写入的机会
	{
		copy_firmware_data(update_len);
		if(check_date_sumer_mcu(FLASH_APP_ADDR,update_len,update_sumer))	//程序校验无误
		{
			memset(&ota_app_para.update_flag[0],FLAG_UPDATE_SUCCESS,4);	//升级成功
		
			bsp_w25qx_write((uint8_t*)&ota_app_para,OTA_APP_INF_ADDR ,sizeof(APP_PARA_TypeDef));
			return 1;
		}
		LOG("copy fw error %d times\r\n",i);
	}
	
	//升级失败
	memset(&ota_app_para.update_flag[0],FLAG_UPDATE_FAIL,4);	

	bsp_w25qx_write((uint8_t*)&ota_app_para,OTA_APP_INF_ADDR ,sizeof(APP_PARA_TypeDef));
	return 0;										//校验失败
}
 
 
 


