﻿/*
 * File      : cmd_table.c
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 */
 

#include <string.h>
#include <stdio.h>
#include "gpio.h"
#include "command.h"

const cmd_tbl_t * command_table[];
extern int bootdelay;

/*
 * 终端清屏
 */
int do_clear(int argc, char * argv[])
{
    printf("\033[2J");	// clear screen
    printf("\033[1;1H"); // 回左上角，1行1列
    return(0);
}
const cmd_tbl_t cmd_clear =
{
    "clear",	1,		0,	do_clear,
    "clear - clear the terminal screen."
};
 /*
 * cmd_version
 */
extern int do_version(int argc, char *argv[]);

const cmd_tbl_t cmd_version =
{
    "version",	1,		1,	do_version,
    "version - print version number"
};

/*
 * 读取唯一ID
 */
/*
 * [COMMAND]
 * 获取芯片UID命令
 *
 * 参数：
 * 命令执行正确函数返回0
 */
int do_uid(int argc, char * argv[])
{
		if(argc != 1)
			return(-1);

		// 获取芯片UID
		printf("\nUID: 0x%08X, 0x%08X, 0x%08X          \n", 
						*(__IO uint32_t*)(UID_BASE), *(__IO uint32_t*)(UID_BASE + 4), *(__IO uint32_t*)(UID_BASE + 8));
		return(0);		
}
const cmd_tbl_t cmd_uid =
{
    "uid",	1,		1,	do_uid,
    "uid - read the unique ID."
};
/*
 * 复位
 */
int do_reset(int argc, char * argv[])
{
//		__set_FAULTMASK(1);
		NVIC_SystemReset();	
    return(0);
}
const cmd_tbl_t cmd_reset =
{
    "reset",	1,		0,	do_reset,
    "reset - systemreset."
};
/*
 *HELP
 */
int do_help(int argc, char *argv[])
{
    int i = 0, j;

    printf("\r\nNAME         USAGE\r\n");
		while(command_table[i] != NULL)
		{
				printf("%s", command_table[i]->name);

				for(j = 0; j < (13 - strlen(command_table[i]->name)); j++)
						printf(" ");
				printf("%s\r\n", command_table[i]->usage);
				i ++;
		}

    return(0);
}

const cmd_tbl_t cmd_help = 
{
	"help",	1,		1,	do_help,
	"help - List all the commands."
};
const cmd_tbl_t cmd_hlp = 
{
	"?",	1,		1,	do_help,
	"? - List all the commands."
}; 
/*
 * 添加命令结构的指针链表，以NULL终结。
 */
const cmd_tbl_t * command_table[] = 
{
	&cmd_version,
	&cmd_clear,
	&cmd_uid,
	&cmd_reset,
	&cmd_help,
	&cmd_hlp,
	NULL
};
 
 
 
 
 



