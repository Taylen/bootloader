/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "rtc.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "version.h"
#include "w25qxx.h"
#include "command.h"
#include "iap.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
/* 进入串口控制台的超时时间*/
#define	MAIN_LOOP_TIMEOUT	1
/* 更新标志*/
#define	UPDATE_FLAG_NUMBER	(0x1a1a1a1a)
int bootdelay = MAIN_LOOP_TIMEOUT;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
char * argv_print_env[] = {"printenv", NULL};
static void  MX_PRINTF(void)
{
	printf("\033[2J");	// clear screen
	printf("\033[1;1H"); // 左上角，1行1列
	printf("/***************************************************************/\r\n");
	printf("                         DTU BootLoader (%s)\r\n",version_string);
	printf("                         www.huabangit.com\r\n");
	printf("/***************************************************************/\r\n");
	printf("authon by zeng\r\n");
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	uint8_t ch;
	uint32_t u32_upgrade_number=0;
	
	unsigned long millisecond, millisecond_2;
	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_USART2_UART_Init();
	MX_SPI3_Init();
	MX_USART1_UART_Init();
	MX_RTC_Init();
  /* USER CODE BEGIN 2 */
	MX_PRINTF();
	
	do_version(1, argv_print_env); //打印系统编译，版本，型号等信息
	HAL_Delay(1000);
	bsp_w25qx_init();	
	
	//MX_W25QXX_READPROTECTDATA(); 

	/* Read upgrade flag */
	bsp_w25qx_read((uint8_t*)&ota_app_para,OTA_APP_INF_ADDR, sizeof(APP_PARA_TypeDef));
	/*
	 * Start Enter main loop
	 */
	printf("\r\nNormal Boot\r\n");
	printf("Hit any key to stop autoboot:  %d(s)", bootdelay);		
	millisecond = millisecond_2 = HAL_GetTick();
	do
	{
		
#if (CONSOLE_TTY == 1)
			if(HAL_UART_Receive(&huart1, &ch, 1, 10) == HAL_OK)
					break;
#endif

#if (CONSOLE_TTY == 2)
			if(HAL_UART_Receive(&huart2, &ch, 1, 10) == HAL_OK)
					break;
#endif
			millisecond_2 = HAL_GetTick();
			if((millisecond_2 - millisecond) > 1000)
			{
					bootdelay --;
					millisecond = millisecond_2;					
					printf("\b\b\b\b%d(s)", bootdelay);
	 
			}
	} while(bootdelay > 0);
	if(bootdelay > 0)
	{
			for(;;)
					main_loop();
	}/* end Enter main loop */

	u32_upgrade_number = (ota_app_para.update_flag[0]<<24)+(ota_app_para.update_flag[1]<<16)
	+(ota_app_para.update_flag[2]<<8)+(ota_app_para.update_flag[3]);
	

	if(u32_upgrade_number != UPDATE_FLAG_NUMBER)		//如果不是升级标志
	{
		
		JumpToAPP();		//退出boot,跳到应用程序
	}
	else
	{
		printf("!!!Start Update\r\n");
		if(firmware_upgrade())
		{
			printf("!!!Update successful  \r\n");
		}
		else
		{
			printf("!!!Update error  \r\n");
		}
		JumpToAPP();		//退出boot,跳到应用程序
	}

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
