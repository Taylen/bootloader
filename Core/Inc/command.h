﻿/*
 * 命令行处理
 * lilian
 */

#ifndef	_COMMAND_H_
#define	_COMMAND_H_

/* 允许命令行输入的最大参数个数 */
#define	CFG_MAXARGS	15


// 行编辑
#define VT_BACKSPACE  0x08
#define VT_CR         0x0D
#define VT_LF         0x0A



/*
 * Monitor Command Table
 */
struct cmd_tbl_s
{
	char	* name;		// Command Name
	int		maxargs;	// maximum number of arguments
	int		repeatable;	// autorepeat allowed ?
	int		(*cmd)(int, char*[]); // Implementation function
	char	* usage;		// Usage message	(short)	
};
typedef struct cmd_tbl_s	cmd_tbl_t;


/*
 * 命令查找
 * 返回NULL表示没有找到匹配的命令
 */
const cmd_tbl_t * find_cmd(const char * cmd);



// main loop
extern void main_loop(void);

#endif

