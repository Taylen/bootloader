﻿/**
  ******************************************************************************
  * @file    usart.h
  * @brief   This file contains all the function prototypes for
  *          the usart.c file
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USART_H__
#define __USART_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */
/* USER CODE BEGIN 3 */
/*
//-------- <<< Use Configuration Wizard in Context Menu >>> ------------------
*/

/* -------------------------- 控制台终端重定向 ----------------------------- */
//
// <h> CONSOLE_TTY
//		<i> Console terminal redirection
//
//		<o> CONSOLE_TTY(USART1 - USART2)
//			<1=> USART1
//			<2=> USART2
//
// </h>
#define CONSOLE_TTY 	1

/* ---------------- serial_putc() & serial_getc() MAX block time in ms ------------------- */
//
// <h> serial_putc() & serial_getc() MAX block time
//		<i>	SERIAL_PUTC_GETC_BLOCK_TIME in ms
//		<o> SERIAL_PUTC_GETC_BLOCK_TIME (1-8) <1-8>
//</h>
#define	SERIAL_PUTC_GETC_BLOCK_TIME	5


/*
 *
 */
#if (APP_DEBUG == 1)
#define	debug_print(...)	printf(__VA_ARGS__)
#define	dbg_msg(...)		{ printf("\n[ FILE: %s LINE: %d FUNCTION: %s ]\n", __FILE__, __LINE__, __FUNCTION__); printf(__VA_ARGS__); }
#else
#define debug_print(...)	{}
#define	dbg_msg(...)		{}
#endif
	
/* USER CODE END Includes */

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void MX_USART1_UART_Init(void);
void MX_USART2_UART_Init(void);

/* USER CODE BEGIN Prototypes */
/*
 *
 */
#if (APP_DEBUG == 1)
#define	debug_print(...)	printf(__VA_ARGS__)
#define	dbg_msg(...)		{ printf("\n[ FILE: %s LINE: %d FUNCTION: %s ]\n", __FILE__, __LINE__, __FUNCTION__); printf(__VA_ARGS__); }
#else
#define debug_print(...)	{}
#define	dbg_msg(...)		{}
#endif
/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif

#endif /* __USART_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
