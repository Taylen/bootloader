 /*
 * File      : iap.h
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * zengxiaohui43@163.com
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 */

#ifndef __IAP_H__
#define __IAP_H__

#include "main.h"
#include "stmflash.h"
#include "w25qxx.h"


/* 开关全局中断的宏 */
#define ENABLE_INT()	__set_PRIMASK(0)	/* 使能全局中断 */
#define DISABLE_INT()	__set_PRIMASK(1)	/* 禁止全局中断 */

#define FLASH_APP_ADDR		0x08020000 //前128k为BOOT

#define OTA_APP_INF_ADDR         0x100000UL //1MB   0x80000  //下载APP信息
#define FAC_APP_INF_ADDR         0x100020UL //   0x80020 //出厂APP信息
#define FIRMWARE_SAVE_ADDR		 0x00   //固件下载存放地址--外部flash
	
#define  FLAG_ENTER_UPDATE     		(0x1a1a1a1a)
#define	 FLAG_UPDATE_SUCCESS		(0x55555555)
#define	 FLAG_UPDATE_FAIL			(0xa5a5a5a5)

/** 
  * @brief APP PARA structure definition  
  */ 
typedef struct
{
  uint8_t update_flag[4];    /*0x1a1a1a1a is update*/
	
  uint8_t soft_ver[5];      /*app soft version*/
	
  uint32_t file_len;      /*app length*/
	
  uint8_t check_sumer[2];     /*check_sumer*/
	
  uint8_t reserved[17];  /*retain*/

}APP_PARA_TypeDef;


extern APP_PARA_TypeDef ota_app_para;

uint8_t firmware_upgrade(void);

void JumpToAPP(void);


#endif

