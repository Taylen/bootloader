﻿/*
 * File      : version.c
 * Change Logs:
 * Date           Author       Notes
 * 2021-05-30     zeng     first version
 */


#ifndef	_VERSION_H_
#define	_VERSION_H_

/*
 * 软件版本: 主.次.补丁
 */
#define	VERSION			1
#define	SUBLEVEL		0
#define	PATCHLEVEL		0
#define	EXTRAVERSION		T	// T:test  ->  B: beta  ->  S: stable 
#define	MONITOR_VERSION 	VERSION.SUBLEVEL.PATCHLEVEL-EXTRAVERSION

//
#define XMK_STR(x)	#x
#define MK_STR(x)	XMK_STR(x)

extern const unsigned char version_string[];

extern const char ver_version;
extern const char ver_sublevel;
extern const char ver_patchlevel;


//
extern int do_version(int argc, char *argv[]);

#endif

